## Présentation du projet
[Retour au menu](../../README.md) 

### **Brief introduction**
The Tacos App est une application développé avec la librairie React pour permettre aux étudiants d'Isitech d'approfondir leur connaissance dans cette technologie.

### **Membre du projet :**
- Samuel LITZLER
- Morgan MERCIER
- Vincent LALO

### **Technologie du projet :**
- [React 17](https://www.npmjs.com/package/react), on utilise la librairie react avec un [router](https://www.npmjs.com/package/react-router-dom) pour la gestion de la navigation
- [Chakra ui](https://www.npmjs.com/package/react-router-dom) : UI imposé
- [Git](https://git-scm.com/) : On a utilisé git pour gérer notre versionning. On a travaillé avec des branches par feature quand on en avait besoin sinon on s'est tenu au courant quand on push sur la branche principale.
- On utilise 

### **Structure du projet**
[Structure du projet](./Structure.md)

### **Couleurs du projets**

On s'est inspiré des couleurs d'UBER EAT.
- vert : ![#3FC060](https://via.placeholder.com/15/3FC060/000000?text=+)` #F03C15`
- noir : ![#162328](https://via.placeholder.com/15/162328/000000?text=+)` #162328`
- blanc : ![#FFFFFF](https://via.placeholder.com/15/FFFFFF/000000?text=+)` #FFFFFF`

### **Comment faire fonctionner le projet ?**
Il faut déjà copier le .env.example et le nommer en .env.local.

Dans la racine du projet, il faut lancer un `npm install` ou un `yarn` pour télécharger les dépendances, puis le lancer avec `npm start`ou `yarn start`.

Pour se connecter il y a un utilisateur qui est mis en place : 
- identifiant : admin@admin.fr
- mdp : admin1234