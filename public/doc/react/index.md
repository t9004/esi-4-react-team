
### Exigences du projet :

- [x] Full Hook 
- [x] Documentation personnelle, sur les notions découvertes cette semaine
- [x] Documentation du projet
- [x] Organisation 'feature based' de la structure projet
- [x] Au minimum un petit workflow git présenté brièvement
- [x] Des appels API...
- [x] Des sideEffects...
- [x] Un store
- [x] Un context
- [x] React Router
- [ ] un hook custom\*
- [x] des composants réutilisables
- [x] un minimum d'esthétique
- [x] signup/login avec store / context 
- [x] signup/login route guard
- [ ] Test unitaire\*

## Documentation du projet :
- [Présentation du projet](./Presentation.md)
- [Structure du projet](./Structure.md)

## Tests unitaires :
Les tests unitaires sont faits avec la librairie de react [React Testing Library](https://testing-library.com/docs/react-testing-library/intro/).
Il s'agit de tests simples sur la liste des restaurants en faisant des tests sur :
* La liste des restaurants
    * Vérifier que la liste des restaurants est bien retournée
    * Vérifier que le nom du restaurant à un nom donné pour un restaurant
* La liste des menus d'un restaurant
    * Vérifier qu'il y a un nombre de menus données pour un restaurant
    * Vérifier qu'un des menus a bien un nom donné pour un menu d'un restaurant

Les tests se lancent avec la commande :
```sh
yarn test
```
ou
```sh
npm test
```