## Structure du projet
[Retour au menu](../../README.md) 
### A typical top-level directory layout

    .
    ├── src     
    │    ├── doc                   # Documentation du projet
    │    ├── files                 # Fichiers de test avant firebase
    │    ├── img                   # images publiques
    │    └── index.html            # Seul fichier html -> SPA 
    ├── src                        # Fichiers sources
    │   ├── __test__               # Dossier avec les différents tests
    │   ├── components          
    │   │   ├── home               # Regroupe les composants UI de la page home  
    │   │   ├── navigation         # Regroupe les composants UI de la navbar      
    │   │   ├── restaurant         # Regroupe les composants UI des restaurants  
    │   │   ├── utils 
    │   │   └── Layout.jsx         # composant qui va gérer le routeur et la structure des pages
    │   ├── context     
    │   │   ├── PanierStore.js     # Store qui va gérer l'état du panier des commandes
    │   │   └── UserContext.js     # Store qui va gérer l'état de la connexion
    │   ├── img                    # dossier avec les img utilisés dans les composants
    │   ├── routes  
    │   │   ├── About.jsx          # Page A propos utilisé dans le layout
    │   │   ├── ConnectedRoute.jsx # Composant qui va permettre de savoir si la route est privée
    │   │   ├── Contact.jsx        # Page de contact
    │   │   ├── Home.jsx           # Page principale avec la carte et la liste des restaurants '/' 
    │   │   ├── Login.jsx          # Page de login '/'   
    │   │   └── NotFound.jsx       # Page d'erreur 
    │   ├── services 
    │   │   ├── restaurants.js     # javascript pur pour les appels fetch à firebase pour les restaurants   
    │   │   └── firebase-config.js # config firebase basique        
    │   ├── style                  # dossier des styles css custom
    │   ├── App.js                 # jsx contenant le provider de chakra ainsi que le layout
    │   └── index.js               # js entrée de l'application
    ├── .env.example               # Fichier
    ├── package.json               # Script et les packages utilisés
    └── README.md                  # Readme du projet  
