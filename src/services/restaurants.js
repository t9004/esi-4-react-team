async function getRestaurants(){
  // On pourrait passer par firebase pour récupérer les restaurants
  // Side effect car on utilise fetch
  let response = await fetch('https://tacosapp-e113a-default-rtdb.europe-west1.firebasedatabase.app/restaurants.json');
  response = await response.json();
  return response;
}

async function getRestaurantById(id){
  let response = await fetch(`https://tacosapp-e113a-default-rtdb.europe-west1.firebasedatabase.app/restaurants/${id}.json`);
  response = await response.json();
  return response;
}

export default {getRestaurants, getRestaurantById};