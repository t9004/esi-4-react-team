import { useState, useContext, useRef, useEffect } from "react";
import {
  Flex,
  Heading,
  Input,
  Button,
  InputGroup,
  Stack,
  InputLeftElement,
  chakra,
  Box,
  Link,
  FormControl,
  FormHelperText,
  InputRightElement,
  Text,
  Center,
  useToast
} from "@chakra-ui/react";

import { FaUserAlt, FaLock } from "react-icons/fa";
import { useNavigate } from "react-router-dom";
import {UserContext} from "../context/UserContext"
import Toast from "../components/utils/Toast";

// Icons pour le formulaire de connexion
const CFaUserAlt = chakra(FaUserAlt);
const CFaLock = chakra(FaLock);

// Composant de la page de login
function Login() {
  // Boolean pour l'affichage du mot de passe
  const [showPassword, setShowPassword] = useState(false);
  // Utilisation du context UserContext pour la connexion
  const {signIn,isConnected} = useContext(UserContext);
  // Trigger le show/hide du champ password
  const handleShowClick = () => setShowPassword(!showPassword);
  // Hook de navigation
  const navigate = useNavigate();
  // Hook pour récupérer les valeurs du formulaire
  const emailInput = useRef();
  const passwordInput = useRef();
  // Hook de chakra pour un toaster
  const toast = useToast();

  const handleLogin =  async (e) => {
    e.preventDefault(); // annuler le comportement de base du onSubmit
    console.log("email",emailInput.current.value);
    console.log("password",passwordInput.current.value);

    // sign in avec firebase
    try {
      const login = await signIn(
        emailInput.current.value,
        passwordInput.current.value
      );
      // Si connexion failed
      if (!login.user.accessToken) {
        toast({
          title: 'Email ou mot de passe incorrect',
          description: '',
          status: 'error',
          duration: 3000,
          isClosable: true,
          position: 'bottom-left',
        })
        //! Ne veut pas fonctionner car pas dans un composant
        // return (
        //   <>
        //     {
        //       Toast({
        //         title:'Email ou mot de passe incorrect',
        //         status: 'error',
        //         description:''
        //       })
        //     }
        //   </>
        // )
      }
      // console.log("login", login);
      // Si connexion ok
      navigate("/");
    } catch {
      toast({
        title: 'Email ou mot de passe incorrect',
        description: '',
        status: 'error',
        duration: 3000,
        isClosable: true,
        position: 'bottom-left',
      })
      //! Ne veut pas fonctionner car pas dans un composant
      // return (
      //   <>
      //     {
            // Toast({
            //   title:'Email ou mot de passe incorrect',
            //   status: 'error',
            //   description:''
            // })
      //     }
      //   </>
      // )
    }
  };
  // Redirige vers / si le mec est déjà connecté
  useEffect(() => {
    if (isConnected()) {
      //! Ne veut pas fonctionner car pas dans un composant
      // return (
      //   <>
      //     {
      //       Toast({
      //         title:'Déjà connecté',
      //         status: 'info',
      //         description:''
      //       })
      //     }
      //   </>
      // )
      navigate("/");
    }
  })
  return (
    <Flex
      flexDirection="column"
      width="100wh"
      height="90vh"
      justifyContent="center"
      alignItems="center"
    >
      <Stack
        flexDir="column"
        mb="2"
        justifyContent="center"
        alignItems="center"
      >
        <Heading color="green.500">Bienvenue ! </Heading>
        <Text color="green.500">Connectez vous pour profiter de la tacos app !</Text>
        {/* Formulaire de login */}
        <Box minW={{ base: "90%", md: "468px" }}>
          <form onSubmit={handleLogin}>
            <Stack
              spacing={4}
              p="1rem"
              backgroundColor="whiteAlpha.200"
              boxShadow="none"
            >
              {/* Email */}
              <FormControl>
                <InputGroup>
                  <InputLeftElement
                    color="green.800"
                    pointerEvents="none"
                    children={<CFaUserAlt color="green.500" />}
                  />
                  <Input required ref={emailInput} type="email" placeholder="email address" />
                </InputGroup>
              </FormControl>
              {/* Mot de passe */}
              <FormControl>
                <InputGroup>
                  <InputLeftElement
                    pointerEvents="none"
                    color="green.800"
                    children={<CFaLock color="green.500" />}
                  />
                  <Input required ref={passwordInput} type={showPassword ? "text" : "password"} placeholder="Password"/>
                  <InputRightElement width="4.5rem">
                    <Button h="1.75rem" size="sm" onClick={handleShowClick}>
                      {showPassword ? "Cacher" : "Voir"}
                    </Button>
                  </InputRightElement>
                </InputGroup>
              </FormControl>
              {/* Bouton "Submit" */}
              <Center>
                <Button
                  borderRadius={5}
                  type="submit"
                  variant="solid"
                  colorScheme="green"
                  width="10rem"
                >
                  Se connecter
                </Button>
              </Center>
            </Stack>
          </form>
        </Box>
      </Stack>
    </Flex>
  );
};

export default Login;