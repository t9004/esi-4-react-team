import { useForm } from 'react-hook-form';
import {
    FormErrorMessage,
    FormLabel,
    FormControl,
    Input,
    Button,
    Box,
    Flex,
    Textarea,
    Heading,
} from '@chakra-ui/react'

// Simple formulaire de contact (non fonctionnel sur l'envoi de mail)
function Contact() {
    /*Utilisation de react-hook-form, présent et proposé par chakra ui
    Cette page permet de submit un formulaire complet / valide et d'afficher en console les erreurs et les données submit*/
    const {
      register,
      handleSubmit,
      formState: { errors, isSubmitting }
    } = useForm();

    //Affiche les données si le formulaire est submit
    const onSubmit = data => {
      console.log('data : ');
      console.log(data);
    }

    //Affichage des potentielles erreurs du formulaire
    console.log('errors : ');
    console.log(errors)

    return (
      <Flex justifyContent={'center'}>
          <Box w={"400px"}>
              <form onSubmit={handleSubmit(onSubmit)}>
                  {/*Prénom*/}
                  <FormControl isInvalid={errors.firstname}>
                      <Heading as='h2' textAlign='center' py='6' size='xl'>Nous contacter</Heading>
                      <FormLabel htmlFor='firstname'>Prénom</FormLabel>
                      <Input
                          id="firstname"
                          type="text"
                          placeholder="Vincent"
                          {...register(
                              "firstname",
                              {required: true, maxLength: 80})}
                      />
                      <FormErrorMessage>
                          {errors.firstname && errors.firstname.message}
                      </FormErrorMessage>
                  </FormControl>
                  {/*Nom*/}
                  <FormControl isInvalid={errors.lastname}>
                      <FormLabel htmlFor='lastname'>Nom</FormLabel>
                      <Input
                          id="lastname"
                          type="text"
                          placeholder="Dupont"
                          {...register(
                              "lastname",
                              {required: true, maxLength: 100})}
                      />
                      <FormErrorMessage>
                          {errors.lastname && errors.lastname.message}
                      </FormErrorMessage>
                  </FormControl>
                  {/*Email*/}
                  <FormControl isInvalid={errors.email}>
                      <FormLabel htmlFor='email'>Email</FormLabel>
                      <Input
                          id="email"
                          type="text"
                          placeholder="example@example.com"
                          {...register("email",
                              {required: true, pattern: /^\S+@\S+$/i})}
                      />
                      <FormErrorMessage>
                          {errors.email && errors.email.message}
                      </FormErrorMessage>
                  </FormControl>
                  {/*Message*/}
                  <FormControl isInvalid={errors.message}>
                      <FormLabel htmlFor='message'>Message</FormLabel>
                      <Textarea
                          id="message"
                          type="text"
                          placeholder="Message"
                          size='lg'
                          {...register("message",
                              {required: true, minLength: 6, maxLength: 12})}
                      />
                      <FormErrorMessage>
                          {errors.message && errors.message.message}
                      </FormErrorMessage>
                  </FormControl>
                  {/*Bouton submit*/}
                  <Button mt={4} colorScheme='teal' isLoading={isSubmitting} type='submit' w={'100%'}>
                      Envoyer
                  </Button>
              </form>
          </Box>
      </Flex>
    );
}

export default Contact;