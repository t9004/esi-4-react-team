import { VStack, Box, Flex, StackDivider } from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import Map from '../components/home/Map';
import RestaurantList from "../components/restaurant/Restaurant";
import datas from '../services/restaurants';

// Ce composant est la route '/' de l'application, il est accéssible seulement si on est connecté et liste les restaurants existants sur la map ainsi que dans un tableau
function Home(){
  const [restaurants, setRestaurants] = useState([]);

  useEffect(() => {
    datas.getRestaurants().then(restaurants => setRestaurants(restaurants))
  }, [])

  return (
    <Box>
      <Flex mt={'20'} justifyContent={'center'} h={'80vh'}>
        <Box w={'50%'}>
          <Map restaurants={restaurants}/>
        </Box>
        <Box w={'460px'} mx={7} py={'2'} ml={'40px'} overflowY={'auto'}>
          <VStack
              divider={<StackDivider borderColor='gray.200' />}
              spacing={4}
              align='stretch'
          >
            { restaurants?.map(restaurant => <RestaurantList restaurant={restaurant} key={restaurant.id} data-testid={'restaurant'}/>) }
          </VStack>
        </Box>
      </Flex>
      </Box>
    )
  }

  

export default Home;