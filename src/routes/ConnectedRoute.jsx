import React, { useContext } from "react";
import { Route, Navigate } from "react-router-dom";
import Toast from "../components/utils/Toast";
import {UserContext} from "../context/UserContext"

// Ce composant permet de gérer les routes qui ont besoin que l'utilisateur soit connecté
const ConnectedRoute = (props) => {
  const {isConnected} = useContext(UserContext);
  if (!isConnected()) {
    Toast({
      title:'Erreur, tu dois être connecté ',
      status: 'error',
      description:''
    });
    return (
      <Navigate to="/login" />
    )
  } else {
    return (
      <>
        {props.composant}
      </>
    )
  }

}

export default ConnectedRoute