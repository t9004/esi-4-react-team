import React from 'react';
import { ChakraProvider,theme } from '@chakra-ui/react';
import Layout from './components/Layout';
import './style/App.css';
import { PanierContextProvider } from './context/PanierStore';

function App() {

  return (
    // On vient emglober le provider de chakra pour pouvoir utiliser ses composants
    <ChakraProvider theme={theme}> 
      <PanierContextProvider>
        <Layout />
      </PanierContextProvider>
    </ChakraProvider>
  );
}

export default App;
