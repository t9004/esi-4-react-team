import { Button, Heading, Text } from '@chakra-ui/react';
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet'
import { GiTacos } from "react-icons/gi";
import { Link } from 'react-router-dom';
// import { IconLeaflet } from './IconLeaflet';


// Ce composant va permettre l'affichage d'une map Leaflet.
// Le composant récupère les restaurants en props et permet de cliquer directement sur les marker de la map pour accéder au restaurant
function Map(props) {
  const {restaurants} = props;

  return (
    <>
      {/* Container de la map Leaflet */}
      <MapContainer center={[45.751986, 4.834744]} zoom={13}>
        {/* Ajout du layer openstreetmap */}
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />

        {
          // Ajout des restaurants sur la map
          restaurants?.map(r => 
            <Marker 
              position={[r.position.latitude, r.position.longitude]} 
              key={r.id}
            >
              {/* Popup lorsqu'on click sur le marker du restaurant */}
              <Popup>
                <Text align='center'>
                  <Heading as='h5' size='sm' mb='2' isTruncated>{ r.nom }</Heading>
                  { r.description }
                </Text>

                <Link to={'restaurant/' + r.id}>
                  {/* Redirection vers la fiche du restaurant */}
                  <Button leftIcon={<GiTacos/>} colorScheme='teal' size='xs'>
                    Commander
                  </Button>
                </Link>
              </Popup>
            </Marker>
            )
        }
      </MapContainer>
    </>
  );
}

export default Map;