import { Badge, Box, Button, Divider, Flex, Heading, Image, Spacer, Text, toast, useToast } from "@chakra-ui/react";
import { useContext } from "react";
import { FaShoppingCart } from "react-icons/fa";
import { getPanierState, panierContext } from "../../context/PanierStore";
// import Toast from "../utils/Toast";

function RestaurantMenu(props) {
  const { menu, restaurantId, restaurantNom } = props;
  const toast = useToast();

  if(!menu){
    return <div>Chargement...</div>
  }

  const [ menus, updateMenus ] = useContext(panierContext);

  const clickMenuHandler = () => {
    // Ajoute le menu au panier et affiche un toast
    updateMenus('MENU_ADD', {...menu, restaurantId, restaurantNom});
    toast({
      title: 'Menu ajouté au panier',
      description: <Box>Le menu {menu.nom} à bien été ajouté au panier</Box>,
      status: 'info',
      duration: 3000,
      isClosable: true,
      position: 'bottom-left',
    })
    //! Ne veut pas fonctionner car pas dans un composant
    //   Toast({
    //     title:'Menu ajouté au panier',
    //     status:'v' ,
    //     description:<Box>Le menu {menu.nom} à bien été ajouté au panier</Box>
    //   });
  }

  // Lien de l'image à récupérer (boucle sur 4 images)
  const imagePath = './../img/menu-' + (menu.menuId % 4) + '.jpg';


  return (
    <Box m='4' _hover={{
      'boxShadow': 'rgb(0 0 0 / 12%) 0px 4px 16px;',
      'transition': 'box-shadow 0.3s ease-in-out 0s;',
      'transitionProperty': 'box-shadow;',
      'transitionDuration': '0.3s;',
      'transitionTimingFunction': 'ease-in-out;',
      'transitionDelay': '0s;'
    }}>
      <Box p='3' m='4'>
        <Heading textAlign='center' as='h3' size='lg' mb='3'>{ menu.nom }</Heading>
        <Flex gap='30' py='4'>
          <Box w='50%'>
            <Image
              boxSize='100%'
              objectFit='cover'
              src={imagePath}
              alt='Image'
              aria-label={'Image de ' + menu.nom}
            />
          </Box>
          <Box w='50%'>
            <Text textAlign='justify'>
              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
            </Text>
          </Box>
        </Flex>
        <Flex>
          <Text fontWeight={'bold'} fontSize='2xl'>
            Prix: {menu.prix}€
          </Text>
          <Spacer/>
          <Button leftIcon={<FaShoppingCart/>} aria-label={'Bouton de ' + menu.nom} colorScheme='teal' variant='solid' onClick={clickMenuHandler}>
            Ajouter au panier
          </Button>
        </Flex>
      </Box>



    </Box>
  );
}

export default RestaurantMenu;