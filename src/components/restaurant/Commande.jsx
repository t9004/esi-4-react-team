import { Box, Button, Divider, Flex, FormControl, FormLabel, Heading, Input, LinkBox, Spacer, Table, TableContainer, Tbody, Text, Th, Thead, Tr, useToast } from "@chakra-ui/react";
import { m } from "framer-motion";
import { useContext } from "react";
import { panierContext } from "../../context/PanierStore";


function Commande(){

  const [ menus, updateMenus ] = useContext(panierContext);
  const toast = useToast();

  // Réinitialise le panier et affiche un toast
  function clickCommande(){
    updateMenus('MENU_RESET');
    toast({
      title: 'Commande éffectuée',
      description: <Box>La commande à bien été passée. Bon appétit !!!</Box>,
      status: 'success',
      duration: 3000,
      isClosable: true,
      position: 'bottom-left',
    })
  }


  // Affichage de la table récapitulative des commandes
  function bluidTableMenu(){

    return (
      <TableContainer>
        <Table>
          <Thead>
            <Tr>
              <Th>Restaurant</Th>
              <Th>Nom</Th>
              <Th>Prix</Th>
            </Tr>
          </Thead>
          <Tbody>
            {
              menus?.map((m, index) => 
                <Tr key={index}>
                  <Th>{ m.restaurantNom }</Th>
                  <Th>{ m.nom }</Th>
                  <Th>{ m.prix }€</Th>
                </Tr>                  
              )
            }
          </Tbody>
        </Table>
      </TableContainer>
    )
  }

  return (
    <Box>
      <Flex gap={50} justify='center'>
        <Box boxShadow='lg' p='5' rounded='md' mt='3' mh='100%' overflowY='auto'>
          <Heading>Récapitulatif de votre commande</Heading>
          <Divider my='3' />
          { bluidTableMenu() }
          <Text fontSize='3xl' fontWeight='bold' textAlign='right' mt='3' mr='3'>
            Total: { menus.reduce((prev, curr) => prev + curr.prix, 0) }€
          </Text>

        </Box>

        {/* Formulaire des informations personnelles */}
        <Box boxShadow='lg' p='5' rounded='md' mt='3'>
          <Heading>Informations personnelles</Heading>
          <Divider my='3' />

          <FormControl>
            <FormLabel htmlFor='email'>Adresse mail</FormLabel>
            <Input id='email' type='email' />
          </FormControl>

          <FormControl>
            <FormLabel htmlFor='nom'>Nom complet</FormLabel>
            <Input id='nom' type='text' />
          </FormControl>

          <FormControl>
            <FormLabel htmlFor='nom'>Télephone</FormLabel>
            <Input id='nom' type='tel' />
          </FormControl>

          <FormControl>
            <FormLabel htmlFor='adr'>Adresse postale</FormLabel>
            <Input id='adr' type='text' />
          </FormControl>
          
          <Button w='100%' mt='4' colorScheme='teal' isDisabled={menus.length === 0} onClick={clickCommande}>
            Commander
          </Button>
        </Box>
      </Flex>
    </Box>
  )
}


export default Commande;