import { Box, Center, Flex, Heading, SimpleGrid, Text } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import datas from "../../services/restaurants";
import RestaurantMenu from "./RestaurantMenu";

function RestaurantFiche(props) {
  const {id} = useParams();

  const [restaurant, setRestaurant] = useState({})

  useEffect(() => {
    if(id){
      // Récupère l'objet du restaurant depuis l'API
      datas.getRestaurantById(id).then(r => setRestaurant(r));
    }
  }, [])
  
  if(!restaurant){
    return <div>Chargement...</div>
  }

  return (
    <div>
      <Flex
        backgroundImage="url('/img/background-tacos.jpeg')"
        backgroundPosition="center"
        backgroundRepeat="no-repeat"
        h={'700px'}
        justifyContent={'center'}
      >
        <Center>
          <Heading textShadow='2px 0 0 #000, -2px 0 0 #000, 0 2px 0 #000, 0 -2px 0 #000, 2px 2px #000, -2px -2px 0 #000, 2px -2px 0 #000, -2px 2px 0 #000' as='h2' size='3xl' color={'white'}>Bienvenue chez { restaurant.nom }</Heading>
        </Center>
      </Flex>

      <Box mt='5' p='2'>
        <Heading as='h2' size='2xl' textAlign={'center'} mb='4'>Les menus</Heading>
        <SimpleGrid columns={2} spacingX='40px' spacingY='20px'>
          {
            // List des différents menus du restaurant
            restaurant.menus?.map(m => <RestaurantMenu restaurantId={id} restaurantNom={restaurant.nom} menu={m} key={m.menuId}/>)
          }
        </SimpleGrid>
      </Box>

    </div>
  )
}

export default RestaurantFiche;