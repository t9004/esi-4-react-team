import React, { useEffect } from "react";
import {
  ChakraProvider,
  Box,
  Text,
  VStack,
  Code,
  Grid,
  theme,
} from "@chakra-ui/react";
import { Link } from 'react-router-dom';


const RestaurantList = (props) => {
  const {id, nom, description} = props.restaurant;

  return (
    <Link to={'restaurant/' + id}>
      <Box key={id} p={3} style={{cursor: "pointer"}} _hover={{ 'backgroundColor': '#33CC66' }} borderRadius='5'>
        <Text as={'b'}>{nom}</Text>
        <Text fontSize='xs'>{description}</Text>
      </Box>
    </Link>
  );
};
export default RestaurantList;
