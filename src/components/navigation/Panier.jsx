import { Box, Button, ButtonGroup, Flex, Icon, IconButton, Menu, MenuButton, MenuDivider, MenuItem, MenuList, MenuOptionGroup, Spacer, Text, useToast } from "@chakra-ui/react";
import { useContext } from "react";
import { panierContext } from "../../context/PanierStore";
import { FaShoppingCart, FaTrash } from "react-icons/fa";
import Toast from "../utils/Toast";
import { BsDot } from "react-icons/bs";
import { Link } from "react-router-dom";


function Panier(){

  const [ menus, updateMenus ] = useContext(panierContext);
  const toast = useToast();

  // Filtre les menus par nom de restaurant
  const listRestaurantsId = [...new Set(menus.map(m => m.restaurantNom))];
  let menusFiltered = listRestaurantsId.map(r => ({
    restaurantNom: r,
    menus: menus.filter(m => m.restaurantNom == r)
  }));

  // Calcul le prix total
  const totalPrix = menus.reduce((prev, curr) => prev + curr.prix, 0)

  function removeMenu(m) {
    // Supprime le menu du panier
    updateMenus('MENU_REMOVE', m)
    toast({
      title: 'Menu supprimé du panier',
      description: <Box>Le menu {m.nom} à bien été supprimé du panier</Box>,
      status: 'info',
      duration: 3000,
      isClosable: true,
      position: 'bottom-left',
    })
    //! Ne veut pas fonctionner car pas dans un composant
    // Toast({
    //   title:'Menu supprimé du panier',
    //   status: 'success',
    //   description:<Box>Le menu {m.nom} à bien été supprimé du panier</Box>
    // });
  }

  
  function buildMenuList() {
    if(menusFiltered.length == 0){
      return <Text textAlign='center'>Panier vide</Text>;
    }

    // Affiche le list des menus par restaurant
    return menusFiltered.map((m, index1) => {
      return (
        <Box key={index1}>
          <MenuOptionGroup title={m.restaurantNom}>
            {
              m.menus.map((m2, index2) => (
                <Flex key={index1 + index2}>
                  <MenuItem as={Box}>
                    { m2.nom }, { m2.prix }€
                    <Spacer/>                    
                    <IconButton colorScheme='red' aria-label="Suppr" icon={<FaTrash />} size='sm' onClick={(e) => removeMenu(m2)} />
                  </MenuItem>
                </Flex>
              ))
            }
          </MenuOptionGroup>
          <MenuDivider/>
        </Box>
      )
    })     

  }



  return (
    <Box mx='3'>
      <Menu>
        <MenuButton as={Button} rightIcon={<FaShoppingCart />}>
          Panier ({menus.length})
        </MenuButton>
        <MenuList maxH='500px' overflowY='auto'>
          {
            buildMenuList()
          }
          {
            menusFiltered.length > 0 ? 
            <Box px='2'>
              <Link to='commande'>
                <Button w='100%' colorScheme='teal' rounded='none'>Commander <Icon as={BsDot} /> { totalPrix } € </Button>
              </Link>
            </Box>
            : ''
          }
        </MenuList>
      </Menu>
    </Box>
  )
}

export default Panier;