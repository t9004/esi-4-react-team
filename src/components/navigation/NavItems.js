const NAV_ITEMS = [
  {
    label: 'Carte',
    href: '/',
  },
  {
    label: 'Contact',
    href: 'contact',
  },
  {
    label: 'A Propos',
    href: 'about',
  }
  // {
  //   label: 'Test menu',
  //   children: [
  //     {
  //       label: 'Explore Design Work',
  //       subLabel: 'Trending Design to inspire you',
  //       href: '#',
  //     },
  //     {
  //       label: 'New & Noteworthy',
  //       subLabel: 'Up-and-coming Designers',
  //       href: '#',
  //     },
  //   ],
  // }
];

export default NAV_ITEMS