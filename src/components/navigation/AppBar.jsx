import {
  Box,
  Flex,
  Text,
  IconButton,
  Button,
  Stack,
  Collapse,
  useColorModeValue,
  useBreakpointValue,
  useDisclosure,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  useToast,
} from '@chakra-ui/react';
import {
  HamburgerIcon,
  CloseIcon,
} from '@chakra-ui/icons';
import { ColorModeSwitcher } from './ColorModeSwitcher';
import MobileNav from './mobile/MobileNav';
import DesktopNav from './desktop/DeskopNav';
import { Link } from 'react-router-dom';
import { UserContext } from '../../context/UserContext';
import { useContext } from 'react';
import { signOut } from 'firebase/auth';
import { useNavigate } from 'react-router-dom';
import { Auth } from '../../services/firebase-config';
import Panier from './Panier';
import Toast from '../utils/Toast';

function AppBar() {
  const { isOpen, onToggle } = useDisclosure();
  const {isConnected} = useContext(UserContext);
  const navigate = useNavigate();
  const toast = useToast();



  const toggleLogOut = async () => {
    try {
      await signOut(Auth)
      navigate("/login")
    } catch {
      toast({
        title: 'Serveur introuvable, êtes vous connecté à internet ?',
        description: '',
        status: 'error',
        duration: 3000,
        isClosable: true,
        position: 'bottom-left',
      })
      //! Ne veut pas fonctionner car pas dans un composant
      // Toast({
      //   title:'Serveur introuvable, êtes vous connecté à internet ?',
      //   status: 'error',
      //   description:''
      // });
    }
  }

  const toggleUserMenu = () => {
    if (isConnected()) {
      return (
        <Button
            display={{ base: 'none', md: 'inline-flex' }}
            onClick={toggleLogOut}
            fontSize={'sm'}
            fontWeight={600}
        >
          Deconnexion
        </Button>
      )
    } else {
      return (
        <Link to={"login"}>
          <Button
            display={{ base: 'none', md: 'inline-flex' }}
            fontSize={'sm'}
            fontWeight={600}
            color={'white'}
            bg={'green.400'}
            _hover={{
              bg: 'green.600',
            }}>
          Se Connecter
          </Button>
        </Link>
      )
    }
  }

  return (
    <Box>
      <Flex
        bg={useColorModeValue('white', 'gray.800')}
        color={useColorModeValue('gray.600', 'white')}
        py={{ base: 2 }}
        px={{ base: 4 }}
        borderBottom={1}
        position={"fixed"}
        height={"60px"}
        width={"100%"}
        borderStyle={'solid'}
        borderColor={useColorModeValue('gray.200', 'gray.900')}
        align={'center'}>
        <Flex
          flex={{ base: 1, md: 'auto' }}
          ml={{ base: -2 }}
          display={{ base: 'flex', md: 'none' }}>
          <IconButton
            onClick={onToggle}
            icon={
              isOpen ? <CloseIcon w={3} h={3} /> : <HamburgerIcon w={5} h={5} />
            }
            variant={'ghost'}
            aria-label={'Toggle Navigation'}
          />
        </Flex>
        <Flex flex={{ base: 1 }} justify={{ base: 'center', md: 'start' }}>
          <Text
            textAlign={useBreakpointValue({ base: 'center', md: 'left' })}
            fontFamily={'heading'}
            color={useColorModeValue('gray.800', 'white')}>
            The Tacos App
          </Text>

          <Flex display={{ base: 'none', md: 'flex' }} ml={10}>
            <DesktopNav />
          </Flex>
        </Flex>

        <Panier></Panier>

        <Stack
          flex={{ base: 1, md: 0 }}
          justify={'flex-end'}
          direction={'row'}
          spacing={6}>
            {toggleUserMenu()}
          <ColorModeSwitcher justifySelf="flex-end" />
        </Stack>
      </Flex>

      <Collapse in={isOpen} animateOpacity>
        <MobileNav />
      </Collapse>
    </Box>
  );
}


export default AppBar