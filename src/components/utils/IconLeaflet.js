import L from 'leaflet';
// non utilisé : le clique sur l'icon ne marche plus

const IconLeaflet = (iconName) => new L.Icon({
    iconUrl: require('../img/' + iconName + '.svg'),
    // iconRetinaUrl: require('../img/' + iconName + '.svg'),
    iconAnchor: null,
    popupAnchor: null,
    shadowUrl: null,
    shadowSize: null,
    shadowAnchor: null,
    iconSize: new L.Point(40, 40),
    className: 'leaflet-div-icon'
});

export { IconLeaflet };