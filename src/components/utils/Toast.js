import { useToast } from "@chakra-ui/react";

const Toast = (props) => {
  const toast = useToast();
  const {title, status, description} = props;
  toast({
    title: title,
    description: description,
    status: status,
    duration: 3000,
    isClosable: true,
    position: 'bottom-left',
  })
}

export default Toast
