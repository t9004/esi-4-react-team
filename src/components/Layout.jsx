import React from "react";
import { BrowserRouter, Link, Route, Routes } from "react-router-dom";
import Contact from "../routes/Contact";
import About from "../routes/About";
import Home from "../routes/Home";
import Login from "../routes/Login";
import AppBar from "./navigation/AppBar";
import NotFound from "../routes/NotFound";
import RestaurantFiche from "./restaurant/RestaurantFiche";
          
import {UserContextProvider} from "../context/UserContext";
import { Container, Flex } from "@chakra-ui/react";
import ConnectedRoute from "../routes/ConnectedRoute";
import Commande from "./restaurant/Commande";

// Ce composant sert à rassembler toutes les routes de l'application

const Layout = () => {
  return (
    <BrowserRouter>
      <UserContextProvider> 
        <Flex>
          <AppBar/>
        </Flex>
        <Container mt="60px" maxW="100%" p="0">
          <Routes>
            {/* Ici on vient utiliser le composant ConnectedRoute qui va fonctionner comme middleware pour la route "/" et donc le composant Home */}
            <Route path="/" element={ <ConnectedRoute composant={<Home />} />} />
            <Route path="restaurant/:id" element={<RestaurantFiche />} />
            <Route path="commande" element={ <ConnectedRoute composant={<Commande />} />} />
            <Route path="login" element={<Login />} />
            <Route path="contact" element={<Contact />} />
            <Route path="about" element={<About />} />
            {/* Pour toutes les autres routes non trouvées, on redirige vers notfound */}
            <Route path="*" element={<NotFound />} />
          </Routes>
        </Container>
      </UserContextProvider>
    </BrowserRouter>
  );
}

export default Layout;
