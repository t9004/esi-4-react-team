// import { createContext } from "react";

import { createContext, useContext, useReducer, useState } from "react";

export const panierContext = createContext([])

export const PanierContextProvider = (props) => {
  const [state, dispatch] = useReducer(panierReducer, []); // Utilise le reducer

  const updateMenus = (actionType, payload) => {
    dispatch({type: actionType, payload}) // Envoie l'action au reducer
  }

  return (
    // Ajout du Provider en tant que composant
    <panierContext.Provider value={[state, updateMenus]}>
      { props.children }
    </panierContext.Provider>
  )
}


export const panierReducer = (state, action) => {
  switch(action.type){
    case 'MENU_ADD': // Ajout d'un menu dans le panier
      return [...state, action.payload]
    case 'MENU_REMOVE': // Enleve un menu du panier
      const index = state.findIndex(m => m.menuId == action.payload.menuId && m.restaurantId == action.payload.restaurantId)
      const newState = [...state];
      newState.splice(index, 1);
      return newState;
    case 'MENU_RESET': // Reinitialise le panier
      return [];
    default:
      break;
  }
}
