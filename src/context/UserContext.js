import { createContext, useState, useEffect } from "react";

import {
  signInWithEmailAndPassword,
  onAuthStateChanged
} from "firebase/auth"

import {Auth} from "../services/firebase-config"

export const UserContext = createContext()

// Ce context vient garder en mémoire le status de la connexion

export const UserContextProvider = (props) => {
  // utilisation de la connexion de firebase
  const signIn = (email, pwd) => signInWithEmailAndPassword(Auth, email, pwd)
  // Retourne un simple boolean de connexion
  const isConnected = () => currentUser == null || currentUser.accessToken  == null ? false : true

  const [currentUser, setCurrentUser] = useState();
  const [loadingData, setLoadingData] = useState(true);
  // console.log("CurrentUser : ", currentUser);

  // Check à chaque changement l'état de la connexion
  useEffect(() => {
    const unsubscribe = onAuthStateChanged(Auth, (currentUser) => {
      setCurrentUser(currentUser)
      setLoadingData(false)
    })
    return unsubscribe;
  }, [])

  // Permet d'exposer les fonctions ou arguments dans l'application
  return (
    <UserContext.Provider value={{currentUser, signIn, isConnected}}>
      {!loadingData && props.children}
    </UserContext.Provider>
  )
}
