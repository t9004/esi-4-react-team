import datas from "../services/restaurants";

test('Check if restaurant recovery with fetch works', async() => {
    const restaurant = await datas.getRestaurantById(0)
    expect(typeof restaurant).toBe('object');
});

test('Check that the restaurant has the given name', async() => {
    const restaurant = await datas.getRestaurantById(0)
    expect(restaurant.nom).toBe('Chamas Tacos');
});

// test('Test failed restaurant is null', async() => {
//     const restaurant = await datas.getRestaurantById(0)
//     expect(typeof restaurant).toBeNull();
// });