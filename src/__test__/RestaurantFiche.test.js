import datas from "../services/restaurants";
import { render, screen } from '@testing-library/react';
import RestaurantMenu from "../components/restaurant/RestaurantMenu";

test('Checks that the restaurant menu has an image', async() => {
    const restaurant = await datas.getRestaurantById(0);
    restaurant.menus?.map(m => {
        render(<RestaurantMenu restaurantId={restaurant.id} restaurantNom={restaurant.nom} menu={m} key={m.menuId}/>);

        const restaurantButton = screen.getByRole('img', { name: 'Image de ' + m.nom});
        expect(restaurantButton);
    });
});

test('Checks that the restaurant menu has a cart button', async() => {
    const restaurant = await datas.getRestaurantById(0);
    restaurant.menus?.map(m => {
        render(<RestaurantMenu restaurantId={restaurant.id} restaurantNom={restaurant.nom} menu={m} key={m.menuId}/>);

        const restaurantButton = screen.getByRole('button', { name: 'Bouton de ' + m.nom});
        expect(restaurantButton);
    });
});