import datas from '../services/restaurants';

test('Check if restaurant at 3 menus', async() => {
    const restaurant = await datas.getRestaurantById(0)
    expect(restaurant.menus.length).toBe(3);
});

test('Check that the menu has the given name', async() => {
    const restaurant = await datas.getRestaurantById(0)
    expect(restaurant.menus[0].nom).toBe('Tacos classique');
});
