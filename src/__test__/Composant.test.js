
import RestaurantFiche from '../components/restaurant/RestaurantFiche';
import Commande from '../components/restaurant/Commande';
import Home from '../routes/Home';
import ConnectedRoute from '../routes/ConnectedRoute';
import Login from '../routes/Login';
import Contact from '../routes/Contact';
import About from '../routes/About';
import NotFound from '../routes/NotFound';
import { MemoryRouter } from 'react-router-dom';

import React from "react";
import {render, screen, waitFor} from '@testing-library/react'
import Layout from '../components/Layout';
import App from '../App';
import '@testing-library/jest-dom'

// simuler les modules de tous les composants vers lesquels le routeur est acheminé
jest.mock('../components/restaurant/RestaurantFiche');
jest.mock('../components/restaurant/Commande');
jest.mock('../routes/Home');
jest.mock('../routes/ConnectedRoute');
jest.mock('../routes/Login');
jest.mock('../routes/Contact');
jest.mock('../routes/About');
jest.mock('../routes/NotFound');

describe('Le composant est', () => {

    test(" - RestaurantFiche", async ()  => {
        RestaurantFiche.mockImplementation(() => <div>RestaurantFiche-PageMock</div>);
        render(<RestaurantFiche/>)
        await waitFor(() => {
            expect(screen.getByText('RestaurantFiche-PageMock')).toBeInTheDocument()
        })
    })

    test(" - Commande", async ()  => {
        Commande.mockImplementation(() => <div>Commande-PageMock</div>);
        render(<Commande/>)
        await waitFor(() => {
            expect(screen.getByText('Commande-PageMock')).toBeInTheDocument()
        })
    })

    test(" - Home", async ()  => {
        Home.mockImplementation(() => <div>Home-PageMock</div>);
        render(<Home/>)
        await waitFor(() => {
            expect(screen.getByText('Home-PageMock')).toBeInTheDocument()
        })
    })

    test(" - ConnectedRoute", async ()  => {
        ConnectedRoute.mockImplementation(() => <div>ConnectedRoute-PageMock</div>);
        render(<ConnectedRoute/>)
        await waitFor(() => {
            expect(screen.getByText('ConnectedRoute-PageMock')).toBeInTheDocument()
        })
    })

    test(" - Login", async ()  => {
        Login.mockImplementation(() => <div>Login-PageMock</div>);
        render(<Login/>)
        await waitFor(() => {
            expect(screen.getByText('Login-PageMock')).toBeInTheDocument()
        })
    })

    test(" - Contact", async ()  => {
        Contact.mockImplementation(() => <div>Contact-PageMock</div>);
        render(<Contact/>)
        await waitFor(() => {
            expect(screen.getByText('Contact-PageMock')).toBeInTheDocument()
        })
    })

    test(" - About", async ()  => {
        About.mockImplementation(() => <div>About-PageMock</div>);
        render(<About/>)
        await waitFor(() => {
            expect(screen.getByText('About-PageMock')).toBeInTheDocument()
        })
    })

    test(" - NotFound", async ()  => {
        NotFound.mockImplementation(() => <div>NotFound-PageMock</div>);
        render(<NotFound/>)
        await waitFor(() => {
            expect(screen.getByText('NotFound-PageMock')).toBeInTheDocument()
        })
    })
})
