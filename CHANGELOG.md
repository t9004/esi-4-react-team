# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.0.0](https://gitlab.com/t9004/esi-4-react-team/compare/v0.2.0...v1.0.0) (2022-04-27)


### Features

* Ajout des lib de test pour le dom + tester si les composants ([6789e86](https://gitlab.com/t9004/esi-4-react-team/commit/6789e86eb6c52eb81d7810ee2ccd8f258d33ef36))
* Ajout lib Jest & écriture d'un test qui doit fail ([da3a8a0](https://gitlab.com/t9004/esi-4-react-team/commit/da3a8a07c8cba4b93ffc7825945af1db25be6fa5))
* ajout travis.yml ([994e806](https://gitlab.com/t9004/esi-4-react-team/commit/994e806860e72de187723b4f58d8ff3474787ce4))
* changelent text bienvenue ([9a9e572](https://gitlab.com/t9004/esi-4-react-team/commit/9a9e5721080ae6c285e2039b60489a76a1a09efe))


### Bug Fixes

* ajout du build heroku directement avec le ci ([ea11090](https://gitlab.com/t9004/esi-4-react-team/commit/ea11090b5221b3741c1e4f63c8c8051141d01846))
* Ajout du dossier build ([fe16e9f](https://gitlab.com/t9004/esi-4-react-team/commit/fe16e9ffd0eada1999a91c0e6adbe5548b8c48da))
* ajout du procfile pour heroku ([4ef6777](https://gitlab.com/t9004/esi-4-react-team/commit/4ef6777806ad0b3fbd9f17f78bd66ed36903e75f))
* build heroku sans push le /build ([40b8f85](https://gitlab.com/t9004/esi-4-react-team/commit/40b8f855019c8c12ddcc79b45072760cce092e54))
* commenter le test bugé ([c756118](https://gitlab.com/t9004/esi-4-react-team/commit/c756118caa0a0755942b44732010f6b6a9b89cfd))
* enleve mot pas correcte ([5193a55](https://gitlab.com/t9004/esi-4-react-team/commit/5193a550d307a4a8a1bfd03d4d5e129f103feca8))
* gem error ci ([4912524](https://gitlab.com/t9004/esi-4-react-team/commit/491252489f89cb5742fdecd178b6e414afe04b64))
* heroku build , ref : https://stackoverflow.com/questions/59850705/how-to-deploy-react-application-to-heroku ([122fcb7](https://gitlab.com/t9004/esi-4-react-team/commit/122fcb7c55a61fee75967b1e04c9a06947e474d5))
* Hotfix master heroku app prod ([52292c5](https://gitlab.com/t9004/esi-4-react-team/commit/52292c5e8c6c42c10d6afd712963a3fa8a6340cc))
* remove travis yml ([68bfce4](https://gitlab.com/t9004/esi-4-react-team/commit/68bfce48f7d1733b40002da93e4b7a6eb71c1a99))
* update procfile to node ([c3c84cd](https://gitlab.com/t9004/esi-4-react-team/commit/c3c84cd480bff7d9c1c9077cd7b883a71e6fc507))
* update procfile with react ([d75c2ed](https://gitlab.com/t9004/esi-4-react-team/commit/d75c2ed8ae8845f93d343e54cf39ffb3a6b620b2))

### [0.3.1](https://gitlab.com/t9004/esi-4-react-team/compare/v0.3.0...v0.3.1) (2022-04-26)

## [0.3.0](https://gitlab.com/t9004/esi-4-react-team/compare/v0.2.1...v0.3.0) (2022-04-26)


### ⚠ BREAKING CHANGES

* une information importante

### Features

* Ajout des lib de test pour le dom + tester si les composants ([6789e86](https://gitlab.com/t9004/esi-4-react-team/commit/6789e86eb6c52eb81d7810ee2ccd8f258d33ef36))
* Ajout lib Jest & écriture d'un test qui doit fail ([da3a8a0](https://gitlab.com/t9004/esi-4-react-team/commit/da3a8a07c8cba4b93ffc7825945af1db25be6fa5))
* une information importante ([502a04f](https://gitlab.com/t9004/esi-4-react-team/commit/502a04f274ffeb789ab3cba3c136e21973b70a3f))


### Bug Fixes

* remove gitlab ci ([7052f92](https://gitlab.com/t9004/esi-4-react-team/commit/7052f92966dedea14bcf8cb3fd966ea4320946f1))
* standard version skip commit ([1cac003](https://gitlab.com/t9004/esi-4-react-team/commit/1cac003d1c56a04eb7da50a8424ddf7da2840fca))

### [0.2.1](https://gitlab.com/t9004/esi-4-react-team/compare/v0.0.2...v0.2.1) (2022-04-26)


### Features

* changelent text bienvenue ([9a9e572](https://gitlab.com/t9004/esi-4-react-team/commit/9a9e5721080ae6c285e2039b60489a76a1a09efe))


### Bug Fixes

* add line space ([2a8110c](https://gitlab.com/t9004/esi-4-react-team/commit/2a8110c1e94a42f3211f44d78187ce91bbb494b6))
* ajout du build heroku directement avec le ci ([ea11090](https://gitlab.com/t9004/esi-4-react-team/commit/ea11090b5221b3741c1e4f63c8c8051141d01846))
* Ajout du dossier build ([fe16e9f](https://gitlab.com/t9004/esi-4-react-team/commit/fe16e9ffd0eada1999a91c0e6adbe5548b8c48da))
* ajout du procfile pour heroku ([4ef6777](https://gitlab.com/t9004/esi-4-react-team/commit/4ef6777806ad0b3fbd9f17f78bd66ed36903e75f))
* build heroku sans push le /build ([40b8f85](https://gitlab.com/t9004/esi-4-react-team/commit/40b8f855019c8c12ddcc79b45072760cce092e54))
* enleve mot pas correcte ([5193a55](https://gitlab.com/t9004/esi-4-react-team/commit/5193a550d307a4a8a1bfd03d4d5e129f103feca8))
* gem error ci ([4912524](https://gitlab.com/t9004/esi-4-react-team/commit/491252489f89cb5742fdecd178b6e414afe04b64))
* heroku build , ref : https://stackoverflow.com/questions/59850705/how-to-deploy-react-application-to-heroku ([122fcb7](https://gitlab.com/t9004/esi-4-react-team/commit/122fcb7c55a61fee75967b1e04c9a06947e474d5))
* hotfix main ([9cceed8](https://gitlab.com/t9004/esi-4-react-team/commit/9cceed89b2816d15921da45a92cd004054ac2f46))
* Mise à jour du ci gitlab pour avoir les phaese : test,build, deploy dev, deploy main ([3725bb2](https://gitlab.com/t9004/esi-4-react-team/commit/3725bb2d0f66fb860f8df42c96c7171a9f2d87bf))
* Mise à jour du ci gitlab pour avoir les phaese : test,build, deploy dev, deploy main ([3c2bbcb](https://gitlab.com/t9004/esi-4-react-team/commit/3c2bbcb30431cedd7dffb297d9a70bd9edd0b8e4))
* unit test require ([bcca41f](https://gitlab.com/t9004/esi-4-react-team/commit/bcca41f828c1d46ceb385096c52ba3f65794bc2f))
* update procfile to node ([c3c84cd](https://gitlab.com/t9004/esi-4-react-team/commit/c3c84cd480bff7d9c1c9077cd7b883a71e6fc507))
* update procfile with react ([d75c2ed](https://gitlab.com/t9004/esi-4-react-team/commit/d75c2ed8ae8845f93d343e54cf39ffb3a6b620b2))

## [0.2.0](https://gitlab.com/t9004/esi-4-react-team/compare/v0.1.6...v0.2.0) (2022-04-26)


### ⚠ BREAKING CHANGES

* une information importante

### Features

* une information importante ([502a04f](https://gitlab.com/t9004/esi-4-react-team/commit/502a04f274ffeb789ab3cba3c136e21973b70a3f))

### [0.1.6](https://gitlab.com/t9004/esi-4-react-team/compare/v0.1.5...v0.1.6) (2022-04-26)

### [0.1.5](https://gitlab.com/t9004/esi-4-react-team/compare/v0.1.4...v0.1.5) (2022-04-26)


### Bug Fixes

* hotfix main ([9cceed8](https://gitlab.com/t9004/esi-4-react-team/commit/9cceed89b2816d15921da45a92cd004054ac2f46))
* Mise à jour du ci gitlab pour avoir les phaese : test,build, deploy dev, deploy main ([3725bb2](https://gitlab.com/t9004/esi-4-react-team/commit/3725bb2d0f66fb860f8df42c96c7171a9f2d87bf))
* Mise à jour du ci gitlab pour avoir les phaese : test,build, deploy dev, deploy main ([3c2bbcb](https://gitlab.com/t9004/esi-4-react-team/commit/3c2bbcb30431cedd7dffb297d9a70bd9edd0b8e4))
* remove gitlab ci ([7052f92](https://gitlab.com/t9004/esi-4-react-team/commit/7052f92966dedea14bcf8cb3fd966ea4320946f1))
* standard version skip commit ([1cac003](https://gitlab.com/t9004/esi-4-react-team/commit/1cac003d1c56a04eb7da50a8424ddf7da2840fca))

### [0.1.1](https://gitlab.com/t9004/esi-4-react-team/compare/v0.1.2...v0.1.1) (2022-04-26)

## 0.1.0 (2022-04-26)


### Features

* ajout lib standard-version ([e38c6c7](https://gitlab.com/t9004/esi-4-react-team/commit/e38c6c7fa6ddaefdd9b6472811cf3d3361845919))


### Bug Fixes

* ajout du script standard-version dans package.json ([323c0ad](https://gitlab.com/t9004/esi-4-react-team/commit/323c0ad135f4415e13bdd228bcdcbf3d02669752))
* bug du merge ([87e9304](https://gitlab.com/t9004/esi-4-react-team/commit/87e93044877842132d3dee71a358c30f61e636f9))
* unit test require ([bcca41f](https://gitlab.com/t9004/esi-4-react-team/commit/bcca41f828c1d46ceb385096c52ba3f65794bc2f))

### [0.0.2](https://gitlab.com/t9004/esi-4-react-team/compare/v0.0.1...v0.0.2) (2022-04-25)

### [0.0.1](https://gitlab.com/t9004/esi-4-react-team/compare/v0.0.0...v0.0.1) (2022-04-25)


### Features

* ajout lib standard-version ([e38c6c7](https://gitlab.com/t9004/esi-4-react-team/commit/e38c6c7fa6ddaefdd9b6472811cf3d3361845919))


### Bug Fixes

* ajout du script standard-version dans package.json ([323c0ad](https://gitlab.com/t9004/esi-4-react-team/commit/323c0ad135f4415e13bdd228bcdcbf3d02669752))
